'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

   return queryInterface.bulkInsert('Plans', [{
    nombre: 'papu rata',
    renta: 'mensual',
    memes: '100 memes',
    costo: '1 btc'
  },{
    nombre: 'papu momazo',
    renta: 'trimestral',
    memes: '250 memes',
    costo: '2.5 btc'
  },{
    nombre: 'papu lolazo',
    renta: 'semestral',
    memes: '750 memes + 100 gif',
    costo: '2.6 btc'
  }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
   return queryInterface.bulkDelete('Plans', null, {});
  }
};
