const Usuarios = require('../models').Usuarios;
const Suscripcion = require('../models').Suscripcion;
const Plan = require('../models').Plan;

module.exports = {
    create(req, res) {
        return Usuarios
        .create({
            nombre: req.body.nombre,
        })
        .then(Usuarios => res.status(201).send(Usuarios))
        .catch(error => res.status(400).send(error));
    },
    list(req, res) {
        return Usuarios
        .findAll({
          include: [{
            model: Suscripcion,
            as: 'suscripcion',
            include: [{ model: Plan,foreignKey: 'planId', }] 
          }],
        })
        .then(Usuarios => res.status(200).send(Usuarios))
        .catch(error => res.status(400).send(error));

        
      },
      retrieve(req, res) {
        return Usuarios
          .findById(req.params.usuarioId)
          .then(Usuarios => {
            if (!Usuarios) {
              return res.status(404).send({
                message: 'Usuario Not Found',
              });
            }
            return res.status(200).send(Usuarios);
          })
          .catch(error => res.status(400).send(error));
      },
      
    update(req, res) {
        return Usuarios
          .findById(req.params.usuarioId)
          .then(Usuarios => {
            if (!Usuarios) {
              return res.status(404).send({
                message: 'Usuario Not Found',
              });
            }
            return Usuarios
              .update({
                nombre: req.body.nombre || Usuarios.nombre,
              })
              .then(() => res.status(200).send(Usuarios))  // Send back the updated todo.
              .catch((error) => res.status(400).send(error));
          })
          .catch((error) => res.status(400).send(error));
      },
      destroy(req, res) {
        return Usuarios
          .findById(req.params.usuarioId)
          .then(Usuarios => {
            if (!Usuarios) {
              return res.status(400).send({
                message: 'Usuario Not Found',
              });
            }
            return Usuarios
              .destroy()
              .then(() => res.status(204).send())
              .catch(error => res.status(400).send(error));
          })
          .catch(error => res.status(400).send(error));
      }
};