const usuarios = require('./usuarios');
const planes = require('./planes');
const suscripcion = require('./suscripcion');

module.exports = {
    usuarios,
    planes,
    suscripcion
};