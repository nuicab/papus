const Plan = require('../models').Plan;

module.exports = {
    create(req, res) {
        return Plan
        .create({
            nombre: req.body.nombre,
            renta: req.body.renta,
            memes: req.body.memes,
            costo: req.body.costo,
        })
        .then(Plan => res.status(201).send(Plan))
        .catch(error => res.status(400).send(error));
    },
    list(req, res) {
        return Plan
          .all()
          .then(Plan => res.status(200).send(Plan))
          .catch(error => res.status(400).send(error));
      },
      retrieve(req, res) {
        return Plan
          .findById(req.params.planId)
          .then(Plan => {
            if (!Plan) {
              return res.status(404).send({
                message: 'plan Not Found',
              });
            }
            return res.status(200).send(Plan);
          })
          .catch(error => res.status(400).send(error));
      },
      
    update(req, res) {
        return Plan
          .findById(req.params.planId)
          .then(Plan => {
            if (!Plan) {
              return res.status(404).send({
                message: 'plan Not Found',
              });
            }
            return Plan
              .update({
                nombre: req.body.nombre || Plan.nombre,
                renta: req.body.renta || Plan.renta,
                memes: req.body.memes || Plan.memes,
                costo: req.body.costo || Plan.costo,
              })
              .then(() => res.status(200).send(Plan))  // Send back the updated todo.
              .catch((error) => res.status(400).send(error));
          })
          .catch((error) => res.status(400).send(error));
      },
      destroy(req, res) {
        return Plan
          .findById(req.params.planId)
          .then(Plan => {
            if (!Plan) {
              return res.status(400).send({
                message: 'Plan Not Found',
              });
            }
            return Plan
              .destroy()
              .then(() => res.status(204).send())
              .catch(error => res.status(400).send(error));
          })
          .catch(error => res.status(400).send(error));
      },
};