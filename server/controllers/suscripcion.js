const Suscripcion = require('../models').Suscripcion;

module.exports = {
    create(req, res) {
        return Suscripcion
        .create({
            usuarioId: req.body.usuarioId,
            planId: req.body.planId,
            fecha_alta: req.body.fecha_alta
        })
        .then(Suscripcion => res.status(201).send(Suscripcion))
        .catch(error => res.status(400).send(error));
    },
    destroy(req, res) {
        return Suscripcion
          .findById(req.params.suscripcionId)
          .then(Suscripcion => {
            if (!Suscripcion) {
              return res.status(400).send({
                message: 'Suscripcion Not Found',
              });
            }
            return Suscripcion
              .destroy()
              .then(() => res.status(204).send())
              .catch(error => res.status(400).send(error));
          })
          .catch(error => res.status(400).send(error));
      }
};