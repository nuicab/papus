const usuariosController = require('../controllers').usuarios;
const planesController = require('../controllers').planes;
const suscripcionController = require('../controllers').suscripcion;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the PAPUS API!',
  }));

  /**
   * End Point Usuario
   */
  app.post('/api/v1/usuarios', usuariosController.create);
  app.get('/api/v1/usuarios', usuariosController.list);
  app.get('/api/v1/usuarios/:usuarioId', usuariosController.retrieve);
  app.put('/api/v1/usuarios/:usuarioId', usuariosController.update);
  app.delete('/api/v1/usuarios/:usuarioId', usuariosController.destroy);

  /**
   * End Point Planes
   */
  app.post('/api/v1/plan', planesController.create);
  app.get('/api/v1/plan', planesController.list);
  app.get('/api/v1/plan/:planId', planesController.retrieve);
  app.put('/api/v1/plan/:planId', planesController.update);
  app.delete('/api/v1/plan/:planId', planesController.destroy);


   /**
    * End Point Subscription
    */
   app.post('/api/v1/suscripcion', suscripcionController.create);
   app.delete('/api/v1/suscripcion/:suscripcionId', planesController.destroy);

};
