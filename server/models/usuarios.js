'use strict';
module.exports = (sequelize, DataTypes) => {
  var Usuarios = sequelize.define('Usuarios', {
    nombre: DataTypes.STRING
  }, {});
  Usuarios.associate = function(models) {
    Usuarios.hasMany(models.Suscripcion, {
      as: 'suscripcion',
      foreignKey: 'usuarioId',
    });
  };
  return Usuarios;
};