'use strict';
module.exports = (sequelize, DataTypes) => {
  var Plan= sequelize.define('Plan', {
    nombre: DataTypes.STRING,
    renta: DataTypes.STRING,
    memes: DataTypes.STRING, 
    costo: DataTypes.STRING,
  }, {});
  Plan.associate = function(models) {
    Plan.hasMany(models.Suscripcion, {
      as: 'suscripcion',
      foreignKey: 'planId',
    });
  };
  return Plan;
};