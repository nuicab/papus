'use strict';
module.exports = (sequelize, DataTypes) => {
  var Suscripcion = sequelize.define('Suscripcion', {
    fecha_alta: DataTypes.STRING
  }, {});
  Suscripcion.associate = function(models) {
    Suscripcion.belongsTo(models.Usuarios, {
      foreignKey: 'usuarioId',
      onDelete: 'CASCADE',
    });

    
    Suscripcion.belongsTo(models.Plan, {
      foreignKey: 'planId',
      onDelete: 'CASCADE',
    });

  };
  return Suscripcion;
};